using UnityEngine;

public class IntroTower : MonoBehaviour {
	
	// events
	private void Start () {
		_currentState = state.start;
	}

	private void Update () {
		switch (_currentState) {

			case state.start: {
				break;
			}

			case state.animating: {
				float timeSince = Time.timeSinceLevelLoad - _activatedAt;
				if (timeSince < _AnimationDuration) {
					Vector3 newPosition = new Vector3();
					newPosition.y = _StartValue + ( _EndValue - _StartValue ) * _Kurva.Evaluate(timeSince / _AnimationDuration);
					transform.position = newPosition;
				} else {
					Vector3 newPosition = new Vector3();
					newPosition.y = _EndValue;
					transform.position = newPosition;
					_BasePadding.SetActive(false);
					_currentState = state.shooting;
				}
				break;
			}

			case state.shooting: {
				_updateTarget();
				if (_target != null) {
					Vector3 toTarget = _target.position - _TowerBody.transform.position;
					Quaternion atTarget = Quaternion.LookRotation(toTarget);
					_TowerBody.rotation = Quaternion.Lerp(_TowerBody.rotation, atTarget, _RotationSpeed * Time.deltaTime);

					_fireTimer -= Time.deltaTime;
					while (_fireTimer <= 0) {
						_fire();
						_fireTimer += 1 / _FireRate;
					}
				}
				break;
			}
		}
	}

	// interface
	public void ACTIVATE () {
		_activatedAt = Time.timeSinceLevelLoad;
		_currentState = state.animating;
	}

	public IntroNode GetParentNode () {
		return _ParentNode;
	}

	public void PlaceOnNode (IntroNode nodeToPlaceOn) {
		_ParentNode = nodeToPlaceOn;
		transform.parent = _ParentNode.transform;
		transform.localPosition = Vector3.zero;
	}

	public void ReturnToParentNode () {
		_ParentNode.PutDownTower(this);
	}

	// parameters
	[Header("Refrences")]
	[SerializeField]
	private Transform _TowerBody;
	[SerializeField]
	private LineRenderer _BeamPrefab;
	[SerializeField]
	private Transform _FirePosition;
	[SerializeField]
	private IntroNode _ParentNode;
	[SerializeField]
	private GameObject _BasePadding;

	[Header("Attributes")]
	[SerializeField]
	private float _Range;
	[SerializeField]
	private float _RotationSpeed;
	[SerializeField]
	private float _FireRate;
	[SerializeField]
	private float _Damage;

	[Header("Activate Animation")]
	[SerializeField]
	private float _StartValue;
	[SerializeField]
	private AnimationCurve _Kurva;
	[SerializeField]
	private float _AnimationDuration;
	[SerializeField]
	private float _EndValue;

	// internals
	private enum state {
		start,
		animating,
		shooting,
	}
	private state _currentState;

	private float _activatedAt;

	private float _fireTimer;
	private Transform _target;

	private void _fire () {
		// create bullet trail
		Vector3 toTarget = _target.transform.position - _FirePosition.position;
		LineRenderer newBulletTrail = Instantiate(_BeamPrefab);
		newBulletTrail.transform.parent = _FirePosition.transform;
		newBulletTrail.SetPosition(0, _FirePosition.position);
		newBulletTrail.SetPosition(1, _FirePosition.position + toTarget);

		_target.GetComponent<IntroEnemy>().Damage(_Damage);
	}

	private void _updateTarget () {
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

		float shortestDistance = Mathf.Infinity;
		GameObject nearestEnemy = null;

		foreach(GameObject enemy in enemies) {
			float distanceToEnemy = Vector3.Distance(enemy.transform.position, _TowerBody.transform.position);
			if (distanceToEnemy < shortestDistance) {
				shortestDistance = distanceToEnemy;
				nearestEnemy = enemy;
			}
		}

		if (nearestEnemy != null && shortestDistance <= _Range) {
			_target = nearestEnemy.transform;
		} else {
			_target = null;
		}
	}

}
