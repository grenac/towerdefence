using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour, IClickable, IEnemyDetectorParent {

	// interface
	public void LoadSpawnData(List<GameObject> newSpawnData) {
		_enemiesToSpawn = newSpawnData;
	}

	public void StartSpawning () {
		_currentState = SpawnerState.WaitingOnClick;
		_ScalePulser.StartPulsing();
	}

	public bool IsIdle() {
		return _currentState == SpawnerState.Idle;
	}

	public void ReportDeath (GameObject enemyThatDied) {
		_spawnedEnemies.Remove(enemyThatDied);
	}

	public void OnPointerClick () {
		if (_currentState == SpawnerState.WaitingOnClick) {
			_currentState = SpawnerState.DoorOpening;
			_ScalePulser.StopPulsing();
		}
	}

	public void AddEnemyToDetected (IEnemy detectedEnemy) {
		_detectedEnemies.Add(detectedEnemy);
	}

	public void ShowDetectors () {
		// NOPE
	}

	public void HideDetectors () {
		// NOPE
	}

	// events
	private void Start () {
		_doorClosedPos = _Door.position;
		_doorOpenPos = _DoorOpen.position;
		_doorCurrentPosOnCurve = 0;

		_currentState = SpawnerState.Idle;

		_spawnedEnemies = new HashSet<GameObject>();

		_detectedEnemies = new HashSet<IEnemy>();
	}

	private void Update () {
		switch (_currentState) {
			case SpawnerState.Idle: {
				break;
			}

			case SpawnerState.DoorOpening: {
				_doorCurrentPosOnCurve += Time.deltaTime * _DoorOpenSpeed;
				_doorCurrentPosOnCurve = Mathf.Clamp01(_doorCurrentPosOnCurve);
				_Door.position = Vector3.Lerp(_doorClosedPos, _doorOpenPos, _DoorAnimationCurve.Evaluate(_doorCurrentPosOnCurve));

				if (_Door.position == _doorOpenPos) {
					_currentState = SpawnerState.Spawning;
				}
				break;
			}

			case SpawnerState.Spawning: {
				if (_enemiesToSpawn.Count != 0) {
					_spawnTimer += Time.deltaTime;
					while (_spawnTimer >= _SpawnPeriod) {
						_spawnTimer -= _SpawnPeriod;
						Vector3 spawnPosition = _SpawnMarker.position;
						Vector3 spawnMarkerRotatedScale = _SpawnMarker.rotation * _SpawnMarker.lossyScale;
						spawnPosition.x += Random.Range(-spawnMarkerRotatedScale.x / 2, spawnMarkerRotatedScale.x / 2);
						spawnPosition.z += Random.Range(-spawnMarkerRotatedScale.z / 2, spawnMarkerRotatedScale.z / 2);
						GameObject newEnemy = Instantiate(_enemiesToSpawn[0], spawnPosition, Quaternion.identity);
						newEnemy.GetComponent<IEnemy>().Init(this, _Waypoints);
						_spawnedEnemies.Add(newEnemy);
						_enemiesToSpawn.RemoveAt(0);
					}
				} else {
					_currentState = SpawnerState.WaitingForSpawnsToLeave;
				}
				break;
			}

			case SpawnerState.WaitingForSpawnsToLeave: {
				if(_detectedEnemies.Count == 0) {
					_currentState = SpawnerState.DoorClosing;
				}
				break;
			}

			case SpawnerState.DoorClosing: {
				_doorCurrentPosOnCurve -= Time.deltaTime * _DoorOpenSpeed;
				_doorCurrentPosOnCurve = Mathf.Clamp01(_doorCurrentPosOnCurve);
				_Door.position = Vector3.Lerp(_doorClosedPos, _doorOpenPos, _DoorAnimationCurve.Evaluate(_doorCurrentPosOnCurve));

				if (_Door.position == _doorClosedPos) {
					_currentState = SpawnerState.WaitingForDeaths;
				}
				break;
			}

			case SpawnerState.WaitingForDeaths: {
				if (_spawnedEnemies.Count == 0) {
					_currentState = SpawnerState.Idle;
				}
				break;
			}
		}
	}

	private void FixedUpdate () {
		// forget all enemies for next frame
		_detectedEnemies.Clear();
	}


	// Parameters
	[SerializeField]
	private Transform _Door;
	[SerializeField]
	private Transform _DoorOpen;
	[SerializeField]
	private AnimationCurve _DoorAnimationCurve;
	[SerializeField]
	private float _DoorOpenSpeed;
	[SerializeField]
	private float _DoorCloseDelay;

	[SerializeField]
	private ScalePulser _ScalePulser;

	[SerializeField]
	private Transform[] _Waypoints;
	[SerializeField]
	private Transform _SpawnMarker;
	[SerializeField]
	private float _SpawnPeriod;


	// internals
	private Vector3 _doorClosedPos;
	private Vector3 _doorOpenPos;
	private float _doorCurrentPosOnCurve;

	private float _spawnTimer;
	private List<GameObject> _enemiesToSpawn;
	private HashSet<GameObject> _spawnedEnemies;

	private HashSet<IEnemy> _detectedEnemies;

	private enum SpawnerState {
		Idle,
		WaitingOnClick,
		DoorOpening,
		Spawning,
		WaitingForSpawnsToLeave,
		DoorClosing,
		WaitingForDeaths,
	}
	private SpawnerState _currentState;
}
