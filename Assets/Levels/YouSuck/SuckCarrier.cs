using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuckCarrier : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		GetComponent<Renderer>().sortingOrder -= 1;

		if (GetComponent<Renderer>().sortingOrder <= int.MinValue) {
			Destroy(gameObject);
		}
	}
}
