using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScalePulser : MonoBehaviour {

	// interface
	public void StartPulsing () {
		_isPulsing = true;
		_animationTime = 0f;
		transform.localScale = _initialScale;
	}

	public void StopPulsing () {
		_isPulsing = false;
		transform.localScale = _initialScale;
	}

	// events
	void Start () {
		_isPulsing = false;
		_animationTime = 0f;
		_initialScale = transform.localScale;
	}
	
	void Update () {
		if (_isPulsing == true) {
			_animationTime += Time.deltaTime;
			transform.localScale = Vector3.Lerp(_initialScale, _TargetScale, _Curve.Evaluate(_animationTime / _AnimationPeriod));
			while (_animationTime > _AnimationPeriod) {
				_animationTime -= _AnimationPeriod;
			}
		}
	}

	// parameters
	[SerializeField]
	private AnimationCurve _Curve;
	[SerializeField]
	private Vector3 _TargetScale;
	[SerializeField]
	private float _AnimationPeriod;

	// internals
	private bool _isPulsing;
	private float _animationTime;
	private Vector3 _initialScale;
}
