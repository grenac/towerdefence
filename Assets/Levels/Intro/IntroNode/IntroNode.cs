using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroNode : MonoBehaviour {

	// interface
	public bool HasTower () {
		if(_currentState == state.idle || _currentState == state.pulsing2) {
			return _IsNodeTwo == false && _tower != null;
		} else {
			return false;
		}
	}

	public void ResetPulseTimer () {
		_pulseTime = 0f;
	}

	public void MakePulseAgain () {
		_currentState = state.pulsing2;
		_pulseTime = -7.5f;
	}

	public void MakeNotPulseAgain () {
		_currentState = state.exitPulsing;
	}

	public IntroTower PickUpTower () {
		IntroTower towerToReturn = _tower;
		_tower = null;
		return towerToReturn;
	}

	public void PutDownTower (IntroTower tower) {
		_tower = tower;
		_tower.PlaceOnNode(this);

		if (_IsNodeTwo == true) {
			_introManager.TowerPlacedOnNodeTwo();
		}
	}

	public void SetColorToHover () {
		_renderer.material.color = _hoverColor;
	}

	public void SetColorToDefault () {
		_renderer.material.color = _startColor;
	}

	public void OnMouseDown () {
		if (_IsNodeTwo == false) {
			if (_currentState == state.pulsing) {
				_introManager.IntroNodeClicked();
				_currentState = state.exitPulsing;
			}
		}
	}

	// events
	private void Start () {
		_currentState = state.start;

		_renderer = GetComponent<Renderer>();
		_startColor = _renderer.material.color;

		_initialScale = transform.localScale;
		_pulseTime = 0;
	}

	private void Update () {
		switch (_currentState) {
			case state.start: {
				_pulseTime += Time.deltaTime;
				while (_pulseTime >= _InitialDelay) {
					_pulseTime -= _InitialDelay;
					_currentState = state.pulsing;
				}
				break;
			}

			case state.pulsing: {
				_pulseTime += Time.deltaTime;
				float curveValue = _PulseCurve.Evaluate(_pulseTime / _PulsePeriod);
				transform.localScale = Vector3.Lerp(_initialScale, _PulseScale, curveValue);

				while (_pulseTime >= _PulsePeriod) {
					_pulseTime -= _PulsePeriod;
				}
				break;
			}

			case state.exitPulsing: {
				_pulseTime += Time.deltaTime;
				float curveValue = _PulseCurve.Evaluate(_pulseTime / _PulsePeriod);
				transform.localScale = Vector3.Lerp(_initialScale, _PulseScale, curveValue);

				while (_pulseTime >= _PulsePeriod) {
					_pulseTime -= _PulsePeriod;
					_currentState = state.idle;
				}
				break;
			}

			case state.pulsing2: {
				_pulseTime += Time.deltaTime;
				float curveValue = _PulseCurve.Evaluate(_pulseTime / _PulsePeriod);
				transform.localScale = Vector3.Lerp(_initialScale, _PulseScale, curveValue);

				while (_pulseTime >= _PulsePeriod) {
					_pulseTime -= _PulsePeriod;
				}
				break;
			}

			case state.idle: {
				break;
			}
		}
	}

	// parameters
	[SerializeField]
	private Color _hoverColor;
	[SerializeField]
	private IntroTower _tower;
	[SerializeField]
	private IntroManager _introManager;

	[SerializeField]
	private float _InitialDelay;
	[SerializeField]
	private Vector3 _PulseScale;
	[SerializeField]
	private float _PulsePeriod;
	[SerializeField]
	private AnimationCurve _PulseCurve;
	[SerializeField]
	private bool _IsNodeTwo;

	// internals
	private enum state {
		start,
		exitPulsing,
		pulsing,
		pulsing2,
		idle,
	}
	private state _currentState;

	private Color _startColor;
	private Renderer _renderer;
	private float _pulseTime;
	private Vector3 _initialScale;
}
