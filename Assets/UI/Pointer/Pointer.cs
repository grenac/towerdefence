using UnityEngine;

public class Pointer : MonoBehaviour {
	
	// events
	private void Update () {
		// ray from camera through mouse pos
		Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

		// raycast follow plane and move pointer to raycasted point
		RaycastHit followPlaneHitInfo;
		if (Physics.Raycast(camRay, out followPlaneHitInfo, Mathf.Infinity, _FollowPlaneMask)) {
			transform.position = followPlaneHitInfo.point;
		}

		// raycast clickables and remember node if one was hit
		RaycastHit clickableHitInfo;
		Physics.Raycast(camRay, out clickableHitInfo, Mathf.Infinity, _ClickableMask);
		Node hitNode = null;
		IClickable hitClickable = null;
		IHoverable hitHoverable = null;
		if (clickableHitInfo.collider != null) {
			hitNode = clickableHitInfo.collider.GetComponent<Node>();
			hitClickable = clickableHitInfo.collider.GetComponent<IClickable>();
			hitHoverable = clickableHitInfo.collider.GetComponent<IHoverable>();
		}

		// node hover
		if (hitHoverable != _lastHoverable) {
			if (hitHoverable != null) {
				hitHoverable.OnPointerEnter();
			}
			if (_lastHoverable != null) {
				_lastHoverable.OnPointerExit();
			}
			_lastHoverable = hitHoverable;
		}

		// left click
		// remember where mouse down was
		if (Input.GetMouseButtonDown(0) == true) {
			_leftMouseDownPosition = transform.position;
			_leftMouseDownNode = hitNode;
		}

		// if mouse is being draged then pick up nodable from node
		if ( Input.GetMouseButton(0) == true
		&& (_leftMouseDownPosition - transform.position).magnitude > _DragMinDistace
		&& _leftMouseDownNode != null
		&& _leftMouseDownNode.IsCarrying() == true
		&& _carriedTransform == null) {
			_carriedTransform = _leftMouseDownNode.PickUpTransform();
			_carriedTransform.transform.parent = transform;
			_carriedTransform.transform.localPosition = Vector3.zero;
		}

		// if mouse up is near where mouse down was then call the OnClicked function
		if (Input.GetMouseButtonUp(0) == true
		&& (_leftMouseDownPosition - transform.position).magnitude <= _DragMinDistace
		&& hitClickable != null) {
			hitClickable.OnPointerClick();
		}

		// when mouse is let go while carrying, place transform at apropriate node
		if (Input.GetMouseButtonUp(0) == true && _carriedTransform != null) {
			if (hitNode != null) {
				if (hitNode.IsCarrying() == true) {
					// switch transforms
					Transform hitNodeCarriedTransform = hitNode.PickUpTransform();
					_leftMouseDownNode.PutDownTransform(hitNodeCarriedTransform);
				}
				hitNode.PutDownTransform(_carriedTransform);
			} else {
				// return to original node
				_leftMouseDownNode.PutDownTransform(_carriedTransform);
			}
			_carriedTransform = null;
		}

		// right click
		// remember where mouse down was
		if (Input.GetMouseButtonDown(1) == true
		&& hitNode != null) {
			_rightMouseDownPosition = transform.position;
			_rightMouseDownNode = hitNode;
		}

		if (Input.GetMouseButton(1) == true
		&& (_rightMouseDownPosition - transform.position).magnitude > _RotateMinDistace
		&& _rightMouseDownNode != null
		&& _rightMouseDownNode.IsCarrying() == true) {
			Quaternion newCarriedTransformRotation = Quaternion.LookRotation(transform.position - _rightMouseDownPosition, Vector3.up);
			_rightMouseDownNode.SetCarriedTransformRotation(newCarriedTransformRotation);
		}

		if (Input.GetMouseButtonUp(1) == true) {
			_rightMouseDownNode = null;
		}
	}

	// parameters
	[SerializeField]
	private LayerMask _FollowPlaneMask;
	[SerializeField]
	private LayerMask _ClickableMask;

	[SerializeField]
	private float _DragMinDistace;
	[SerializeField]
	private float _RotateMinDistace;

	// internals
	private Vector3 _leftMouseDownPosition;
	private Vector3 _rightMouseDownPosition;

	private IHoverable _lastHoverable;

	private Node _leftMouseDownNode;
	private Node _rightMouseDownNode;

	private Transform _carriedTransform;

}
