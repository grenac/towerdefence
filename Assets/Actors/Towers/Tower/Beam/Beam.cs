using UnityEngine;

public class Beam : MonoBehaviour {
	
	// events
	void Start () {
		_renderer = GetComponent<Renderer>();
		if (_renderer != null) {
			_initialAlpha = _renderer.material.color.a;
		}
		_timeCreated = Time.time;
	}

	void Update () {
		float timeAlive = Time.time - _timeCreated;

		if (_Fade == true && _renderer != null) {
			Color color = _renderer.material.color;
			color.a = _initialAlpha * ( 1 - timeAlive / _Duration );
			_renderer.material.color = color;
		}

		if (timeAlive > _Duration) {
			Destroy(gameObject);
		}
	}

	// properties
	[SerializeField]
	private float _Duration;
	[SerializeField]
	private bool _Fade;

	// internals
	private float _initialAlpha;
	private Renderer _renderer;
	private float _timeCreated;
}
