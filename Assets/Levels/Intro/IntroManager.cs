using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroManager : MonoBehaviour {

	// properties
	[SerializeField]
	private IntroTower _Turret;
	[SerializeField]
	private IntroPathAnimator[] _Paths;
	[SerializeField]
	private IntroPillarAnimator _Pillar;
	[SerializeField]
	private IntroSpawnerAnimator _Spawner;
	[SerializeField]
	private IntroPhaseTwoAnimator _phaseTwo;
	[SerializeField]
	private IntroPhaseTwoAnimator _phaseTwoCamera;
	[SerializeField]
	private IntroPhaseTwoAnimator _phaseTwoNode;
	[SerializeField]
	private IntroNode _phaseOneNode;
	[SerializeField]
	private IntroNode _phaseTwoNodeForRealThisTime;
	[SerializeField]
	private IntroSpawner _SpawnerForReal;
	[SerializeField]
	private IntroCam _Camera;

	// interface
	public void IntroNodeClicked () {
		if (_currentState == state.start) {
			_Turret.ACTIVATE();
			_Pillar.ACTIVATE();
			_Spawner.ACTIVATE();
			foreach (IntroPathAnimator path in _Paths) {
				path.ACTIVATE();
			}
			_Camera.ZoomOut();
			_currentState = state.nodeClicked;
		}
	}

	public void FirstWaveKilled () {
		_phaseTwo.ACTIVATE();
		_phaseTwoCamera.ACTIVATE();
		_phaseTwoNode.ACTIVATE();

		_phaseOneNode.MakePulseAgain();
		_phaseTwoNodeForRealThisTime.ResetPulseTimer();
	}

	public void SecondWaveKilled () {
		SceneManager.LoadScene("Main");
	}

	public void TowerPlacedOnNodeTwo() {
		_phaseOneNode.MakeNotPulseAgain();
		_phaseTwoNodeForRealThisTime.MakeNotPulseAgain();
		_SpawnerForReal.StartSpawning();
	}

	// events
	private void Start () {
		_currentState = state.start;
	}

	// internals
	private enum state {
		start,
		nodeClicked,
	}
	private state _currentState;
}
