using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroCam : MonoBehaviour {

	[Header("Zoom Out Animation")]
	[SerializeField]
	private float _animationDuration;
	[SerializeField]
	private Vector3 _startPosition;
	[SerializeField]
	private AnimationCurve _positionCurve;
	[SerializeField]
	private Vector3 _endPosition;
	[SerializeField]
	private float _startSize;
	[SerializeField]
	private AnimationCurve _sizeCurve;
	[SerializeField]
	private float _endSize;

	private bool _animationActive;
	private float _activatedAt;

	private void Start () {
		_animationActive = false;
	}

	private void Update () {
		if (_animationActive == true) {
			float timeSince = Time.timeSinceLevelLoad - _activatedAt;
			if (timeSince < _animationDuration) {
				transform.position = _startPosition + ( _endPosition - _startPosition ) * _positionCurve.Evaluate(timeSince / _animationDuration);
				GetComponent<Camera>().orthographicSize = _startSize + ( _endSize - _startSize ) * _positionCurve.Evaluate(timeSince / _animationDuration);
			} else {
				transform.position = _endPosition;
				GetComponent<Camera>().orthographicSize = _endSize;
				_animationActive = false;
			}
		}
	}

	public void ZoomOut () {
		_activatedAt = Time.timeSinceLevelLoad;
		_animationActive = true;
	}

}
