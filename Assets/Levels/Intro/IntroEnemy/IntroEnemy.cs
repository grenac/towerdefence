using UnityEngine;

public class IntroEnemy : MonoBehaviour {

	[SerializeField]
	private float _speed;
	[SerializeField]
	private float _maxHp;
	[SerializeField]
	private Vector3 _minimumScale;

	private IntroSpawner _parentSpawner;
	private Transform[] _waypoints;

	private Transform _target;

	private int _waypointIndex;
	private float _currentHp;
	private Vector3 _initialScale;


	// interface
	public void Init (IntroSpawner newParentSpawner, Transform[] newWaypoints) {
		_parentSpawner = newParentSpawner;

		_waypoints = newWaypoints;
		_waypointIndex = 0;
		_target = _waypoints[_waypointIndex];
	}

	public void Damage (float damage) {
		_currentHp -= damage;
		transform.localScale =  Vector3.Lerp(_minimumScale, _initialScale, _currentHp / _maxHp);
		if (_currentHp <= 0) {
			_die();
		}
	}

	public void CollidedWithPillar () {
		_die();
	}

	// events
	private void Start () {
		_currentHp = _maxHp;
		_initialScale = transform.localScale;
	}

	private void Update () {
		Vector3 vectorToPoint = _target.position - transform.position;
		float frameSpeed = _speed * Time.deltaTime;
		if (frameSpeed < vectorToPoint.magnitude) {
			transform.position += vectorToPoint.normalized * frameSpeed;
		} else {
			transform.position = _target.transform.position;
			_waypointIndex++;
			if (_waypointIndex < _waypoints.Length) {
				_target = _waypoints[_waypointIndex];
			}
		}
	}

	// internal
	private void _die () {
		_parentSpawner.ReportDeath(gameObject);
		Destroy(gameObject);
	}
}

