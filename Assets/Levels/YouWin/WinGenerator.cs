﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinGenerator : MonoBehaviour {

	[SerializeField]
	private GameObject _winProjectile;
	[SerializeField]
	private Sprite[] _winSprites;

	[SerializeField]
	private float _InitialWinPeriod;

	[SerializeField]
	private Vector3 _initialVelocity;
	[SerializeField]
	private float _initialVelocitySpread;
	[SerializeField]
	private float _initialAngularVelocitySpread;

	private float _winTimer;
	private float _winPeriod;

	void Start () {
		_winTimer = 0;
		_winPeriod = _InitialWinPeriod * Random.Range(0.8f, 1.2f);
	}

	void Update () {
		_winTimer += Time.deltaTime;
		while (_winTimer >= _winPeriod) {
			GameObject newWinProjectile = Instantiate(_winProjectile, transform.position, Quaternion.identity, transform);
			newWinProjectile.GetComponent<SpriteRenderer>().sprite = _winSprites[Random.Range(0, _winSprites.Length)];
			newWinProjectile.GetComponent<Rigidbody>().velocity = _initialVelocity + _initialVelocitySpread * Random.insideUnitSphere;
			newWinProjectile.GetComponent<Rigidbody>().angularVelocity = _initialAngularVelocitySpread * Random.insideUnitSphere;

			_winTimer -= _winPeriod;
			_winPeriod = _InitialWinPeriod * Random.Range(0.8f, 1.2f);
		}
	}
}
