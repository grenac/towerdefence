using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetector : MonoBehaviour {

	// interface
	public void Show () {
		GetComponent<Renderer>().enabled = true;
	}

	public void Hide () {
		GetComponent<Renderer>().enabled = false;
	}

	// Events
	private void OnTriggerStay (Collider other) {
		IEnemy otherEnemy = other.GetComponent<IEnemy>();
		if (otherEnemy != null) {
			_Parent.GetComponent<IEnemyDetectorParent>().AddEnemyToDetected(otherEnemy);
		}
	}

	// properties
	[SerializeField]
	private GameObject _Parent;
}
