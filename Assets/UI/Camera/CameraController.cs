using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	[SerializeField]
	private float _panSpeed;
	[SerializeField]
	private float _panBorder;
	[SerializeField]
	private bool _mouseScrollEnabled;
	[SerializeField]
	private float _minCamSize;
	[SerializeField]
	private float _maxCamSize;

	private Vector3 _panUp;
	private Vector3 _panRight;
	private Camera _cam;
	
	private void Start () {
		_calcCamPanVectors(out _panUp, out _panRight);
		_cam = GetComponent<Camera>();
	}

	void Update () {
		float panAmount = _cam.orthographicSize * _panSpeed * Time.deltaTime;

		if (Input.GetAxis("Vertical") > 0f || _mouseScrollEnabled && Input.mousePosition.y >= Screen.height - _panBorder) {
			transform.Translate(panAmount * _panUp, Space.World);
		}
		if (Input.GetAxis("Vertical") < -0f || _mouseScrollEnabled && Input.mousePosition.y <= _panBorder) {
			transform.Translate(-panAmount * _panUp, Space.World);
		}
		if (Input.GetAxis("Horizontal") > 0f || _mouseScrollEnabled && Input.mousePosition.x >= Screen.width - _panBorder) {
			transform.Translate(panAmount * _panRight, Space.World);
		}
		if (Input.GetAxis("Horizontal") < -0f || _mouseScrollEnabled && Input.mousePosition.x <= _panBorder ) {
			transform.Translate(-panAmount * _panRight, Space.World);
		}

		float newCamSize = _cam.orthographicSize * ( 1 - Input.GetAxis("Mouse ScrollWheel") );
		_cam.orthographicSize = Mathf.Clamp(newCamSize, _minCamSize, _maxCamSize);
	}

	private void _calcCamPanVectors (out Vector3 panUp, out Vector3 panRight) {
		panUp = new Vector3(transform.up.x, 0f, transform.up.z).normalized;
		panRight = new Vector3(transform.right.x, 0f, transform.right.z).normalized;
	}
}
