using UnityEngine;

public class Jumper : MonoBehaviour, IEnemy {

	// interface
	public void Init (EnemySpawner newParentSpawner, Transform[] newWaypoints) {
		_parentSpawner = newParentSpawner;

		_waypoints = newWaypoints;
		_waypointIndex = 0;
		currentMovePosition = _waypoints[_waypointIndex].position + new Vector3(
			Random.Range(-waypointPositionRandomOffset.x, waypointPositionRandomOffset.x),
			Random.Range(-waypointPositionRandomOffset.y, waypointPositionRandomOffset.y),
			Random.Range(-waypointPositionRandomOffset.z, waypointPositionRandomOffset.z));
	}

	public void Damage (float damage) {
		_currentHp -= damage;
		currentBaseScale = Vector3.Lerp(_MinimumScale, _initialScale, _currentHp / _MaxHp);
		if (_currentHp <= 0) {
			_die();
		}
	}

	public void CollidedWithPillar () {
		_die();
	}

	public float GetTimeAlive () {
		return _timeAlive;
	}

	public bool IsAlive () {
		return _currentHp > 0;
	}

	public Transform GetTransform () {
		return transform;
	}

	// events
	private void Start () {
		_currentHp = _MaxHp;
		currentBaseScale = _initialScale = transform.localScale;
		_timeAlive = 0f;
		jumpChargeTime = 0f;
	}

	private void FixedUpdate () {
		_timeAlive += Time.deltaTime;
		Vector3 toWaypoint = currentMovePosition - transform.position;
		Vector3 toWaypointMinusUp = toWaypoint - Vector3.up * Vector3.Dot(Vector3.up, toWaypoint);

		// is grounded
		bool isGrounded = false;
		float altitude = Mathf.Infinity;
		RaycastHit hitInfo;
		if (Physics.Raycast(transform.position, Vector3.down, out hitInfo, Mathf.Infinity, groundMask)) {
			altitude = hitInfo.distance - transform.lossyScale.y / 2;
			if (altitude < groundDetectionDistance) {
				isGrounded = true;
			}
		}

		// velocity
		GetComponent<Rigidbody>().velocity *= Mathf.Exp(( 1 - moveDrag ) * Time.deltaTime);
		if (isGrounded == false) {
			GetComponent<Rigidbody>().velocity += moveAcceleration * toWaypointMinusUp.normalized * Time.deltaTime;
		}

		// jumping
		if (isGrounded == true && jumped == false) {
			jumpChargeTime += Time.deltaTime;
			if (jumpChargeTime > jumpChargePeriod) {
				GetComponent<Rigidbody>().velocity += jumpVerticalSpeed * Vector3.up;
				GetComponent<Rigidbody>().velocity += jumpHorizontalSpeed * toWaypointMinusUp.normalized;
				jumpChargeTime = 0f;
				jumped = true;
			}
		} else if (isGrounded == false) {
			jumped = false;
		}

		// rotation
		if (isGrounded) {
			Vector3 newDir = Vector3.RotateTowards(transform.forward, toWaypointMinusUp, rotationSpeed * Time.deltaTime, 0f);
			transform.rotation = Quaternion.LookRotation(newDir, Vector3.up);
		}

		// scale
		Vector3 totalScale = currentBaseScale;
		
		// by altitude
		float totalAltitudeScaleFactor = 1 + altitudeScaleFactor * altitude;
		totalScale.x /= totalAltitudeScaleFactor;
		totalScale.y *= ( totalAltitudeScaleFactor * totalAltitudeScaleFactor );
		totalScale.z /= totalAltitudeScaleFactor;
		
		// by jump timer
		float totalJumpScaleFactor = 1 + jumpScaleFactor * jumpAnimationCurve.Evaluate(jumpChargeTime / jumpChargePeriod) ;
		totalScale.x *= totalJumpScaleFactor;
		totalScale.y /= ( totalJumpScaleFactor * totalJumpScaleFactor );
		totalScale.z *= totalJumpScaleFactor;

		transform.localScale = totalScale;

		// update waypoint
		if (isGrounded && toWaypointMinusUp.magnitude < waypointProximityLimit) {
			_waypointIndex++;
			if (_waypointIndex < _waypoints.Length) {
				currentMovePosition = _waypoints[_waypointIndex].position + new Vector3(
					Random.Range(-waypointPositionRandomOffset.x, waypointPositionRandomOffset.x),
					Random.Range(-waypointPositionRandomOffset.y, waypointPositionRandomOffset.y),
					Random.Range(-waypointPositionRandomOffset.z, waypointPositionRandomOffset.z));
			}
		}
	}

	//properties
	[SerializeField]
	private float _MaxHp;
	[SerializeField]
	private Vector3 _MinimumScale;

	[SerializeField]
	private float moveAcceleration;
	[SerializeField]
	private float moveDrag;
	[SerializeField]

	private float jumpChargePeriod;
	[SerializeField]
	private float jumpVerticalSpeed;
	[SerializeField]
	private float jumpHorizontalSpeed;
	[SerializeField]
	private float altitudeScaleFactor;
	[SerializeField]
	private float jumpScaleFactor;
	[SerializeField]
	private AnimationCurve jumpAnimationCurve;

	[SerializeField]
	private float rotationSpeed;

	[SerializeField]
	private float waypointProximityLimit;
	[SerializeField]
	private Vector3 waypointPositionRandomOffset;

	[SerializeField]
	private LayerMask groundMask;
	[SerializeField]
	private float groundDetectionDistance;


	// internal
	private EnemySpawner _parentSpawner;
	private Transform[] _waypoints;
	private Vector3 currentMovePosition;
	private int _waypointIndex;
	private float _currentHp;
	private Vector3 _initialScale;
	private float _timeAlive;
	private float jumpChargeTime;
	private Vector3 currentBaseScale;
	private bool jumped;

	private void _die () {
		_parentSpawner.ReportDeath(gameObject);
		Destroy(gameObject);
	}
}

