using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroPillar : MonoBehaviour {


	// events
	private void Start () {
		_currentHp = _maxHp;
		_initialHpBarScale = _hpBar.localScale;
	}

	private void Update () {
		Vector3 newScale = _hpBar.localScale;
		newScale.x = _initialHpBarScale.x * _currentHp / _maxHp;
		_hpBar.localScale = newScale;
	}

	private void OnTriggerEnter (Collider other) {
		if (other.GetComponent<IntroEnemy>() != null) {
			_currentHp -= 1f;
			other.GetComponent<IntroEnemy>().CollidedWithPillar();
			if (_currentHp <= 0) {
				SceneManager.LoadScene("YouSuck");
			}
		}
	}

	// parameters
	[SerializeField]
	private Transform _hpBar;
	[SerializeField]
	private float _maxHp;

	// internals
	private float _currentHp;
	private Vector3 _initialHpBarScale;

}
