using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour, IClickable, IHoverable {

	// interface
	public bool IsCarrying () {
		return _CarriedTransform != null;
	}

	public Transform PickUpTransform () {
		Transform transformToReturn = _CarriedTransform;
		_CarriedTransform = null;
		return transformToReturn;
	}

	public void PutDownTransform (Transform transformToPlace) {
		_CarriedTransform = transformToPlace;
		_CarriedTransform.parent = transform;
		_CarriedTransform.localPosition = Vector3.zero;
	}

	public void SetCarriedTransformRotation (Quaternion newRotation) {
		_CarriedTransform.localRotation = newRotation;
		float dotForward = Vector3.Dot(_CarriedTransform.forward, Vector3.forward);
		float dotRight = Vector3.Dot(_CarriedTransform.forward, Vector3.right);
		if (Mathf.Abs(dotForward) > Mathf.Abs(dotRight)) {
			if (dotForward > 0) {
				_CarriedTransform.localRotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);
			} else {
				_CarriedTransform.localRotation = Quaternion.LookRotation(Vector3.back, Vector3.up);
			}
		} else {
			if (dotRight > 0) {
				_CarriedTransform.localRotation = Quaternion.LookRotation(Vector3.right, Vector3.up);
			} else {
				_CarriedTransform.localRotation = Quaternion.LookRotation(Vector3.left, Vector3.up);
			}
		}
	}

	public void OnPointerClick() {
		if (_CarriedTransform != null) {
			IActivatable activatable = _CarriedTransform.GetComponent<IActivatable>();
			if (activatable != null) {
				activatable.Activate();
			}
		}
	}

	public void OnPointerEnter () {
		_renderer.material.color = _HoverColor;
		if (_CarriedTransform != null) {
			IEnemyDetectorParent carriedEnemyDetector = _CarriedTransform.GetComponent<IEnemyDetectorParent>();
			if (carriedEnemyDetector != null) {
				carriedEnemyDetector.ShowDetectors();
			}
		}
	} 

	public void OnPointerExit () {
		_renderer.material.color = _startColor;
		if (_CarriedTransform != null) {
			IEnemyDetectorParent carriedEnemyDetector = _CarriedTransform.GetComponent<IEnemyDetectorParent>();
			if (carriedEnemyDetector != null) {
				carriedEnemyDetector.HideDetectors();
			}
		}
	}

	// events
	private void Start () {
		_renderer = GetComponent<Renderer>();
		_startColor = _renderer.material.color;
	}

	// parameters
	[SerializeField]
	private Color _HoverColor;
	[SerializeField]
	private Transform _CarriedTransform;

	// state
	private Color _startColor;
	private Renderer _renderer;
	
}
