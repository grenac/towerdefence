using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuckGenerator : MonoBehaviour {

	[SerializeField]
	private GameObject _suckCarrier;
	[SerializeField]
	private Sprite[] _suckSprites;

	[SerializeField]
	private float _suckPeriod;

	[SerializeField]
	private float _spread;

	private float _winTimer;

	void Start () {
		_winTimer = 0;
	}

	void Update () {
		_winTimer += Time.deltaTime;
		while (_winTimer >= _suckPeriod) {
			GameObject newSuckCarrier = Instantiate(_suckCarrier);
			newSuckCarrier.GetComponent<SpriteRenderer>().sprite = _suckSprites[Random.Range(0, _suckSprites.Length)];
			newSuckCarrier.transform.position = _spread * Random.insideUnitSphere;
			newSuckCarrier.transform.Rotate(Vector3.forward, Random.Range(-10f, 10f));

			_winTimer -= _suckPeriod;
		}
	}
}
