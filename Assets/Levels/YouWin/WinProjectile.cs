using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinProjectile : MonoBehaviour {

	[SerializeField]
	private GameObject _winShrapnel;
	[SerializeField]
	private Sprite[] _winSprites;
	[SerializeField]
	private float _timeToLive;
	[SerializeField]
	private float _initialVelocitySpread;
	[SerializeField]
	private float _initialAngularVelocitySpread;
	[SerializeField]
	private int _numShrapnel;
	[SerializeField]
	private float _shrapnelScale;


	private float _timerUntillDestructionBecomesMuchMoreThenImmenentHowEverDoYouSpellThat;

	private void Start () {
		_timerUntillDestructionBecomesMuchMoreThenImmenentHowEverDoYouSpellThat = 0;
		_timeToLive *= Random.Range(0.8f, 1.2f);
	}


	private void Update () {
		_timerUntillDestructionBecomesMuchMoreThenImmenentHowEverDoYouSpellThat += Time.deltaTime;

		if (_timerUntillDestructionBecomesMuchMoreThenImmenentHowEverDoYouSpellThat >= _timeToLive) {
			for (int i = 0; i < _numShrapnel; i++) {
				GameObject newWinShrapnel = Instantiate(_winShrapnel, transform.position, Random.rotation);
				newWinShrapnel.transform.localScale = _shrapnelScale * Vector3.one;
				//newWinShrapnel.GetComponent<SpriteRenderer>().sprite = _winSprites[Random.Range(0, _winSprites.Length)];
				newWinShrapnel.GetComponent<SpriteRenderer>().sprite = GetComponent<SpriteRenderer>().sprite;
				newWinShrapnel.GetComponent<Rigidbody>().velocity = _initialVelocitySpread * Random.insideUnitSphere;
				newWinShrapnel.GetComponent<Rigidbody>().angularVelocity = _initialAngularVelocitySpread * Random.insideUnitSphere;
			}
			Destroy(gameObject);
		}
	}

}
