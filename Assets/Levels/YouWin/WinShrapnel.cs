using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinShrapnel : MonoBehaviour {

	[SerializeField]
	private float _timeToLive;

	private float _timerUntillDestructionBecomesMuchMoreThenImmenentHowEverDoYouSpellThat;

	private void Start () {
		_timerUntillDestructionBecomesMuchMoreThenImmenentHowEverDoYouSpellThat = 0;
	}

	private void Update () {
		_timerUntillDestructionBecomesMuchMoreThenImmenentHowEverDoYouSpellThat += Time.deltaTime;

		if (_timerUntillDestructionBecomesMuchMoreThenImmenentHowEverDoYouSpellThat >= _timeToLive) {
			Destroy(gameObject);
		}
	}
}
