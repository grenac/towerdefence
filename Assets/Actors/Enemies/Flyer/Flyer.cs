using UnityEngine;

public class Flyer : MonoBehaviour, IEnemy {

	// interface
	public void Init (EnemySpawner newParentSpawner, Transform[] newWaypoints) {
		_parentSpawner = newParentSpawner;

		_waypoints = newWaypoints;
		_waypointIndex = 0;
		currentMovePosition = _waypoints[_waypointIndex].position + new Vector3(
			Random.Range(-waypointPositionRandomOffset.x, waypointPositionRandomOffset.x),
			Random.Range(-waypointPositionRandomOffset.y, waypointPositionRandomOffset.y),
			Random.Range(-waypointPositionRandomOffset.z, waypointPositionRandomOffset.z));
	}

	public void Damage (float damage) {
		_currentHp -= damage;
		transform.localScale =  Vector3.Lerp(_MinimumScale, _initialScale, _currentHp / _MaxHp);
		if (_currentHp <= 0) {
			_die();
		}
	}

	public void CollidedWithPillar () {
		_die();
	}

	public float GetTimeAlive () {
		return _timeAlive;
	}

	public bool IsAlive () {
		return _currentHp > 0;
	}

	public Transform GetTransform () {
		return transform;
	}

	// events
	private void Start () {
		_currentHp = _MaxHp;
		_initialScale = transform.localScale;
		_timeAlive = 0f;
		flying = false;
	}

	private void FixedUpdate () {
		_timeAlive += Time.deltaTime;
		Vector3 toWaypoint = currentMovePosition - transform.position;
		Vector3 toWaypointMinusUp = toWaypoint - Vector3.up * Vector3.Dot(Vector3.up, toWaypoint);

		// velocity
		GetComponent<Rigidbody>().velocity *= Mathf.Exp(( 1 - moveDrag ) * Time.deltaTime);
		GetComponent<Rigidbody>().velocity += moveAcceleration * toWaypointMinusUp.normalized * Time.deltaTime;

		// flying
		if (flying == true) {
			GetComponent<Rigidbody>().velocity += flyAcceleration * Vector3.up * Time.deltaTime;
			if (transform.position.y > maxFlyHeight) {
				flying = false;
			}
		} else {
			if (transform.position.y < minFallHeight) {
				flying = true;
			}
		}

		// rotation
		Vector3 newDir = Vector3.RotateTowards(transform.forward, toWaypointMinusUp, rotationSpeed * Time.deltaTime, 0f);
		transform.rotation = Quaternion.LookRotation(newDir, Vector3.up);

		// update waypoint
		if (toWaypointMinusUp.magnitude < waypointProximityLimit) {
			_waypointIndex++;
			if (_waypointIndex < _waypoints.Length) {
				currentMovePosition = _waypoints[_waypointIndex].position + new Vector3(
					Random.Range(-waypointPositionRandomOffset.x, waypointPositionRandomOffset.x),
					Random.Range(-waypointPositionRandomOffset.y, waypointPositionRandomOffset.y),
					Random.Range(-waypointPositionRandomOffset.z, waypointPositionRandomOffset.z));
			}
		}
	}

	//properties
	[SerializeField]
	private float _MaxHp;
	[SerializeField]
	private Vector3 _MinimumScale;
	[SerializeField]
	private float moveAcceleration;
	[SerializeField]
	private float moveDrag;
	[SerializeField]
	private float flyAcceleration;
	[SerializeField]
	private float minFallHeight;
	[SerializeField]
	private float maxFlyHeight;
	[SerializeField]
	private float rotationSpeed;
	[SerializeField]
	private float waypointProximityLimit;
	[SerializeField]
	private Vector3 waypointPositionRandomOffset;


	// internal
	private EnemySpawner _parentSpawner;
	private Transform[] _waypoints;
	private int _waypointIndex;
	private Vector3 currentMovePosition;
	private float _currentHp;
	private Vector3 _initialScale;
	private float _timeAlive;
	private bool flying;

	private void _die () {
		_parentSpawner.ReportDeath(gameObject);
		Destroy(gameObject);
	}
}

