using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroSpawner : MonoBehaviour {

	// events
	private void Start () {
		_doorClosedPos = _door.localPosition;
		_doorOpenPos = _doorOpen.localPosition;
		_doorCurrentPosOnCurve = 0;

		_currentState = SpawnerState.Idle;

		_spawnedEnemies = new HashSet<GameObject>();

		_wavesToSpawn = new List<List<GameObject>>();
		_wavesToSpawn.Add(_Wave_1);
		_wavesToSpawn.Add(_Wave_2);

		LoadSpawnData(_wavesToSpawn[0]);
		_wavesToSpawn.RemoveAt(0);
		_spawnTimer = _spawnPeriod;
	}

	private void Update () {
		switch (_currentState) {

			case SpawnerState.Idle:
			break;

			case SpawnerState.DoorOpening: {
				_doorCurrentPosOnCurve += Time.deltaTime * _doorOpenSpeed;
				_doorCurrentPosOnCurve = Mathf.Clamp01(_doorCurrentPosOnCurve);
				_door.localPosition = Vector3.Lerp(_doorClosedPos, _doorOpenPos, _kurva.Evaluate(_doorCurrentPosOnCurve));

				if (_door.localPosition == _doorOpenPos) {
					_currentState = SpawnerState.Spawning;
				}
				break;
			}

			case SpawnerState.Spawning: {
				if (_enemiesToSpawn.Count != 0) {
					_spawnTimer += Time.deltaTime;
					while (_spawnTimer >= _spawnPeriod) {
						_spawnTimer -= _spawnPeriod;
						GameObject newEnemy = Instantiate(_enemiesToSpawn[0], _spawnMarker.position, Quaternion.identity, transform);
						newEnemy.GetComponent<IntroEnemy>().Init(this, _waypoints);
						_spawnedEnemies.Add(newEnemy);
						_enemiesToSpawn.RemoveAt(0);
					}
				} else {
					_currentState = SpawnerState.DoorClosing;
				}
				break;
			}

			case SpawnerState.DoorClosing: {
				_doorCurrentPosOnCurve -= Time.deltaTime * _doorOpenSpeed;
				_doorCurrentPosOnCurve = Mathf.Clamp01(_doorCurrentPosOnCurve);
				_door.localPosition = Vector3.Lerp(_doorClosedPos, _doorOpenPos, _kurva.Evaluate(_doorCurrentPosOnCurve));

				if (_door.localPosition == _doorClosedPos) {
					_currentState = SpawnerState.WaitingForDeaths;
				}
				break;
			}

			case SpawnerState.WaitingForDeaths: {
				if (_spawnedEnemies.Count == 0) {
					_currentState = SpawnerState.Idle;
					if (_wavesToSpawn.Count != 0) {
						LoadSpawnData(_wavesToSpawn[0]);
						_wavesToSpawn.RemoveAt(0);
						_Manager.FirstWaveKilled();
					} else {
						_Manager.SecondWaveKilled();
					}
				}
				break;
			}
		}
	}


	// interface
	public void Activate () {
		StartSpawning();
	}

	public void LoadSpawnData(List<GameObject> newSpawnData) {
		_enemiesToSpawn = newSpawnData;
	}

	public void StartSpawning () {
		_currentState = SpawnerState.DoorOpening;
	}

	public bool IsIdle() {
		return _currentState == SpawnerState.Idle;
	}

	public void ReportDeath (GameObject enemyThatDied) {
		_spawnedEnemies.Remove(enemyThatDied);
	}


	// parameter
	[SerializeField]
	private IntroManager _Manager;

	[SerializeField]
	private Transform _door;
	[SerializeField]
	private Transform _doorOpen;
	[SerializeField]
	private AnimationCurve _kurva;
	[SerializeField]
	private float _doorOpenSpeed;
	[SerializeField]
	private float _doorCloseDelay;

	[SerializeField]
	private Transform[] _waypoints;
	[SerializeField]
	private Transform _spawnMarker;
	[SerializeField]
	private float _spawnPeriod;
	[SerializeField]
	private List<GameObject> _Wave_1;
	[SerializeField]
	private List<GameObject> _Wave_2;


	// internals
	private Vector3 _doorClosedPos;
	private Vector3 _doorOpenPos;
	private float _doorCurrentPosOnCurve;

	private float _spawnTimer;
	private List<List<GameObject>> _wavesToSpawn;
	private List<GameObject> _enemiesToSpawn;
	private HashSet<GameObject> _spawnedEnemies;

	private enum SpawnerState {
		Idle,
		Spawning,
		DoorOpening,
		DoorClosing,
		WaitingForDeaths,
	}
	private SpawnerState _currentState;

}
