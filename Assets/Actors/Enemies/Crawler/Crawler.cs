using UnityEngine;

public class Crawler : MonoBehaviour, IEnemy {

	// interface
	public void Init (EnemySpawner newParentSpawner, Transform[] newWaypoints) {
		_parentSpawner = newParentSpawner;

		_waypoints = newWaypoints;
		_waypointIndex = 0;
		currentMovePosition = _waypoints[_waypointIndex].position + new Vector3(
			Random.Range(-waypointPositionRandomOffset.x, waypointPositionRandomOffset.x),
			Random.Range(-waypointPositionRandomOffset.y, waypointPositionRandomOffset.y),
			Random.Range(-waypointPositionRandomOffset.z, waypointPositionRandomOffset.z));
	}

	public void Damage (float damage) {
		_currentHp -= damage;
		currentBaseScale =  Vector3.Lerp(_MinimumScale, _initialScale, _currentHp / _MaxHp);
		if (_currentHp <= 0) {
			_die();
		}
	}

	public void CollidedWithPillar () {
		_die();
	}

	public float GetTimeAlive () {
		return _timeAlive;
	}

	public bool IsAlive () {
		return _currentHp > 0;
	}

	public Transform GetTransform () {
		return transform;
	}

	// events
	private void Start () {
		_currentHp = _MaxHp;
		currentBaseScale = _initialScale = transform.localScale;
		_timeAlive = 0f;
		moveCurvePosition = 0f;
	}

	private void FixedUpdate () {
		_timeAlive += Time.deltaTime;
		moveCurvePosition += Time.deltaTime;
		if (moveCurvePosition >= movePeriod) {
			moveCurvePosition -= movePeriod;
		}
		Vector3 toWaypoint = currentMovePosition - transform.position;
		Vector3 toWaypointMinusUp = toWaypoint - Vector3.up * Vector3.Dot(Vector3.up, toWaypoint);

		// velocity
		GetComponent<Rigidbody>().velocity *= Mathf.Exp(( 1 - moveDrag ) * Time.deltaTime);
		GetComponent<Rigidbody>().velocity += moveAcceleration * moveCurve.Evaluate(moveCurvePosition / movePeriod) * toWaypointMinusUp.normalized * Time.deltaTime;

		// scale
		float scaleFactor = 1 + moveScaleFactor * GetComponent<Rigidbody>().velocity.magnitude;
		Vector3 finalScale = currentBaseScale;
		finalScale.x /= scaleFactor;
		finalScale.y /= scaleFactor;
		finalScale.z *= ( scaleFactor * scaleFactor );
		transform.localScale = finalScale;

		// rotation
		Vector3 newDir = Vector3.RotateTowards(transform.forward, toWaypointMinusUp, rotationSpeed * Time.deltaTime, 0f);
		transform.rotation = Quaternion.LookRotation(newDir, Vector3.up);

		// update waypoint
		if (toWaypointMinusUp.magnitude < waypointProximityLimit) {
			_waypointIndex++;
			if (_waypointIndex < _waypoints.Length) {
				currentMovePosition = _waypoints[_waypointIndex].position + new Vector3(
					Random.Range(-waypointPositionRandomOffset.x, waypointPositionRandomOffset.x),
					Random.Range(-waypointPositionRandomOffset.y, waypointPositionRandomOffset.y),
					Random.Range(-waypointPositionRandomOffset.z, waypointPositionRandomOffset.z));
			}
		}
	}

	//properties
	[SerializeField]
	private float _MaxHp;
	[SerializeField]
	private Vector3 _MinimumScale;
	[SerializeField]
	private float moveAcceleration;
	[SerializeField]
	private float moveDrag;
	[SerializeField]
	private AnimationCurve moveCurve;
	[SerializeField]
	private float movePeriod;
	[SerializeField]
	private float moveScaleFactor;
	[SerializeField]
	private float rotationSpeed;
	[SerializeField]
	private float waypointProximityLimit;
	[SerializeField]
	private Vector3 waypointPositionRandomOffset;

	// internal
	private EnemySpawner _parentSpawner;
	private Transform[] _waypoints;
	private int _waypointIndex;
	private Vector3 currentMovePosition;
	private float _currentHp;
	private Vector3 _initialScale;
	private Vector3 currentBaseScale;
	private float _timeAlive;
	private float moveCurvePosition;

	private void _die () {
		_parentSpawner.ReportDeath(gameObject);
		Destroy(gameObject);
	}
}

