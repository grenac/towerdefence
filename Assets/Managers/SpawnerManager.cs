using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnerManager : MonoBehaviour {

	[SerializeField]
	private EnemySpawner _levelSpawner;

	[SerializeField]
	private List<GameObject> _wave_1;
	[SerializeField]
	private List<GameObject> _wave_2;
	[SerializeField]
	private List<GameObject> _wave_4;
	[SerializeField]
	private List<GameObject> _wave_6;
	[SerializeField]
	private List<GameObject> _wave_10;
	[SerializeField]
	private List<GameObject> _wave_12;

	private int _waveIndex;
	private List<List<GameObject>> _spawnData;

	void Start () {
		_spawnData = new List<List<GameObject>>();

		_spawnData.Add(_wave_1);
		_spawnData.Add(_wave_2);
		_spawnData.Add(_wave_4);
		_spawnData.Add(_wave_6);
		_spawnData.Add(_wave_10);
		_spawnData.Add(_wave_12);

		_waveIndex = 0;
		_levelSpawner.LoadSpawnData(_spawnData[_waveIndex]);
		_levelSpawner.StartSpawning();
	}


	private void Update () {

		if (_levelSpawner.IsIdle()) {
			_waveIndex++;
			if (_waveIndex >= _spawnData.Count) {
				SceneManager.LoadScene("YouWin");
				return;
			}
			_levelSpawner.LoadSpawnData(_spawnData[_waveIndex]);
			_levelSpawner.StartSpawning();
		}

	}

}
