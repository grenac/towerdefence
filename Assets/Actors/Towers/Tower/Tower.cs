using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour, IEnemyDetectorParent {

	// interface
	public void AddEnemyToDetected (IEnemy detectedEnemy) {
		_detectedEnemies.Add(detectedEnemy);
	}

	public void ShowDetectors () {
		foreach(EnemyDetector enemyDetector in _EnemyDetectors) {
			enemyDetector.Show();
		}
	}

	public void HideDetectors () {
		foreach (EnemyDetector enemyDetector in _EnemyDetectors) {
			enemyDetector.Hide();
		}
	}

	// events
	private void Start () {
		_fireTimer = 0;
		_targetEnemy = null;
		_detectedEnemies = new HashSet<IEnemy>();
	}

	private void Update () {
		// set body rotation
		Quaternion targetRotation;
		if (_targetEnemy != null && _targetEnemy.IsAlive()) {
			Vector3 toTarget = _targetEnemy.GetTransform().position - _TowerBody.transform.position;
			targetRotation = Quaternion.LookRotation(toTarget, Vector3.up);
			_TowerBody.rotation = Quaternion.Lerp(_TowerBody.rotation, targetRotation, _RotationSpeed * Time.deltaTime);
		} else {
			targetRotation = Quaternion.identity;
			_TowerBody.localRotation = Quaternion.Lerp(_TowerBody.localRotation, targetRotation, _RotationSpeed * Time.deltaTime);
		}
	}

	private void FixedUpdate () {
		// get new target
		_targetEnemy = null;
		IEnemy maxTimeAliveEnemy = null;
		foreach (IEnemy enemy in _detectedEnemies) {
			if (maxTimeAliveEnemy == null || enemy.GetTimeAlive() > maxTimeAliveEnemy.GetTimeAlive()) {
				maxTimeAliveEnemy = enemy;
			}
		}
		if (maxTimeAliveEnemy != null) {
			_targetEnemy = maxTimeAliveEnemy;
		}

		// update firing system
		_fireTimer -= Time.deltaTime;
		_fireTimer = Mathf.Clamp(_fireTimer, 0, _FirePeriod);
		if (_targetEnemy != null && _targetEnemy.IsAlive()) {
			while (_fireTimer <= 0) {
				_fireTimer += _FirePeriod;
				// fire
				Vector3 toTarget = _targetEnemy.GetTransform().position - _FirePosition.position;
				LineRenderer newBulletTrail = Instantiate(_BeamPrefab);
				newBulletTrail.transform.parent = _FirePosition.transform;
				newBulletTrail.SetPosition(0, _FirePosition.position);
				newBulletTrail.SetPosition(1, _FirePosition.position + toTarget);
				_targetEnemy.Damage(_Damage);
			}
		}

		// forget all enemies for next frame
		_detectedEnemies.Clear();
	}

	// parameters
	[Header("Refrences")]
	[SerializeField]
	private Transform _TowerBody;
	[SerializeField]
	private LineRenderer _BeamPrefab;
	[SerializeField]
	private Transform _FirePosition;
	[SerializeField]
	private EnemyDetector[] _EnemyDetectors;

	[Header("Attributes")]
	[SerializeField]
	private float _RotationSpeed;
	[SerializeField]
	private float _FirePeriod;
	[SerializeField]
	private float _Damage;

	// internals
	private float _fireTimer;
	private IEnemy _targetEnemy;
	private HashSet<IEnemy> _detectedEnemies;

}
