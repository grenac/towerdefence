using UnityEngine;

public class IntroPointer : MonoBehaviour {

	// parameters
	[SerializeField]
	private LayerMask _FollowPlaneMask;
	[SerializeField]
	private LayerMask _NodesMask;
	[SerializeField]
	private float _DragMinDistace;

	// state
	private Vector3 _mouseDownPosition;
	private IntroNode _lastFrameNode;
	private IntroTower _towerCarried;

	// events
	private void Update () {
		// ray from camera through mouse pos
		Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

		// raycast follow plane
		RaycastHit followPlaneHitInfo;
		if (Physics.Raycast(camRay, out followPlaneHitInfo, Mathf.Infinity, _FollowPlaneMask)) {
			transform.position = followPlaneHitInfo.point;
		}

		// raycast nodes
		RaycastHit nodeHitInfo;
		Physics.Raycast(camRay, out nodeHitInfo, Mathf.Infinity, _NodesMask);
		IntroNode hitNode;
		if (nodeHitInfo.collider != null) {
			hitNode = nodeHitInfo.collider.GetComponent<IntroNode>();
		} else {
			hitNode = null;
		}

		// node hover
		if (hitNode != _lastFrameNode) {
			if (_lastFrameNode != null) {
				_lastFrameNode.SetColorToDefault();
			}
			if (hitNode != null) {
				hitNode.SetColorToHover();
			}
		}

		// mouse logic
		if (Input.GetMouseButtonDown(0) == true) {
			_mouseDownPosition = nodeHitInfo.point;
			if (hitNode != null) {
				hitNode.OnMouseDown();
			}
		}

		if (Input.GetMouseButton(0) == true
			&& ( _mouseDownPosition - nodeHitInfo.point ).magnitude > _DragMinDistace
			&& _lastFrameNode != null
			&& _lastFrameNode.HasTower() == true
			&& _towerCarried == null) {
			_towerCarried = _lastFrameNode.PickUpTower();
			_towerCarried.transform.parent = transform;
			_towerCarried.transform.localPosition = Vector3.zero;
		}

		if (Input.GetMouseButtonUp(0) == true && _towerCarried != null) {
			if (hitNode != null) {
				if (hitNode.HasTower() == true) {
					IntroTower nodeTower = hitNode.PickUpTower();
					IntroNode carriedTowerNode = _towerCarried.GetParentNode();
					carriedTowerNode.PutDownTower(nodeTower);
				}
				hitNode.PutDownTower(_towerCarried);
			} else {
				_towerCarried.ReturnToParentNode();
			}
			_towerCarried = null;
		}

		// remember node for next frame
		_lastFrameNode = hitNode;
	}
}
