﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyDetectorParent {
	void AddEnemyToDetected (IEnemy detectedEnemy);
	void ShowDetectors ();
	void HideDetectors ();
}
