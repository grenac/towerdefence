using UnityEngine;

public class Enemy : MonoBehaviour, IEnemy {

	// interface
	public void Init (EnemySpawner newParentSpawner, Transform[] newWaypoints) {
		_parentSpawner = newParentSpawner;

		_waypoints = newWaypoints;
		_waypointIndex = 0;
		_currentWaypoint = _waypoints[_waypointIndex];
	}

	public void Damage (float damage) {
		_currentHp -= damage;
		transform.localScale =  Vector3.Lerp(_MinimumScale, _initialScale, _currentHp / _MaxHp);
		if (_currentHp <= 0) {
			_die();
		}
	}

	public void CollidedWithPillar () {
		_die();
	}

	public float GetTimeAlive () {
		return _timeAlive;
	}

	public bool IsAlive () {
		return _currentHp > 0;
	}

	public Transform GetTransform () {
		return transform;
	}

	// events
	private void Start () {
		_currentHp = _MaxHp;
		_initialScale = transform.localScale;
		_timeAlive = 0f;
	}

	private void Update () {
		_timeAlive += Time.deltaTime;

		Vector3 vectorToPoint = _currentWaypoint.position - transform.position;
		float frameSpeed = _Speed * Time.deltaTime;
		if (frameSpeed < vectorToPoint.magnitude) {
			transform.position += vectorToPoint.normalized * frameSpeed;
		} else {
			transform.position = _currentWaypoint.transform.position;
			_waypointIndex++;
			if (_waypointIndex < _waypoints.Length) {
				_currentWaypoint = _waypoints[_waypointIndex];
			}
		}
	}

	//properties
	[SerializeField]
	private float _Speed;
	[SerializeField]
	private float _MaxHp;
	[SerializeField]
	private Vector3 _MinimumScale;

	// internal
	private EnemySpawner _parentSpawner;
	private Transform[] _waypoints;
	private Transform _currentWaypoint;
	private int _waypointIndex;
	private float _currentHp;
	private Vector3 _initialScale;
	private float _timeAlive;

	private void _die () {
		_parentSpawner.ReportDeath(gameObject);
		Destroy(gameObject);
	}
}

