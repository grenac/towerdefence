using UnityEngine;

public class IntroBeam : MonoBehaviour {

	// properties
	[SerializeField]
	private float _duration;
	[SerializeField]
	private bool _fade;

	// internals
	private float _initialAlpha;
	private Renderer _renderer;
	private float _timeCreated;

	// events
	void Start () {
		_renderer = GetComponent<Renderer>();
		if (_renderer != null) {
			_initialAlpha = _renderer.material.color.a;
		}
		_timeCreated = Time.time;
	}

	void Update () {
		float timeAlive = Time.time - _timeCreated;

		if (_fade == true && _renderer != null) {
			Color color = _renderer.material.color;
			color.a = _initialAlpha * ( 1 - timeAlive / _duration );
			_renderer.material.color = color;
		}

		if (timeAlive > _duration) {
			Destroy(gameObject);
		}
	}
}
