using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionAnimator : MonoBehaviour {

	// events
	void Start () {
		_currentState = State.start;
		_timeActivated = 0;
		_initialPosition = transform.localPosition;
	}

	void Update () {
		switch (_currentState) {

			case State.start: {
				break;
			}

			case State.animating: {
				float timeSince = Time.timeSinceLevelLoad - _timeActivated;
				if (timeSince > _StartDelay) {
					if (timeSince < _Duration + _StartDelay) {
						float curveValue = _Curve.Evaluate(( timeSince - _StartDelay ) / _Duration);
						transform.localPosition = Vector3.LerpUnclamped(_initialPosition, _TargetPosition, curveValue);
					} else {
						transform.localPosition = _TargetPosition;
						_currentState = State.end;
					}
				}
				break;
			}

			case State.end: {
				break;
			}
		}
	}

	// interface
	public void Animate () {
		_timeActivated = Time.timeSinceLevelLoad;
		_currentState = State.animating;
	}

	// parameters
	[SerializeField]
	private Vector3 _TargetPosition;
	[SerializeField]
	private AnimationCurve _Curve;
	[SerializeField]
	private float _StartDelay;
	[SerializeField]
	private float _Duration;

	// internals
	private enum State {
		start,
		animating,
		end,
	}
	private State _currentState;

	private Vector3 _initialPosition;
	private float _timeActivated;

}
