using System.Collections.Generic;
using UnityEngine;

public class Beamer : MonoBehaviour, IEnemyDetectorParent, IActivatable {

	// interface
	public void AddEnemyToDetected (IEnemy detectedEnemy) {
		_detectedEnemies.Add(detectedEnemy);
	}

	public void ShowDetectors () {
		foreach (EnemyDetector enemyDetector in _EnemyDetectors) {
			enemyDetector.Show();
		}
	}

	public void HideDetectors () {
		foreach (EnemyDetector enemyDetector in _EnemyDetectors) {
			enemyDetector.Hide();
		}
	}

	public void Activate () {
		if (_state == State.waitingOnActivation) {
			foreach (IEnemy enemy in _detectedEnemies) {
				enemy.Damage(_Damage);
			}
			_PistonTopPulser.StopPulsing();
			_state = State.charging;
			_slamTimer = 0f;
		}
	}

	// events
	private void Start () {
		_slamTimer = 0;
		_detectedEnemies = new HashSet<IEnemy>();
		_pistonOriginalPosition = _Piston.localPosition;
		_state = State.charging;
	}

	private void Update () {
		float curveValue = _SlamReloadCurve.Evaluate(_slamTimer / _SlamPeriod);
		_Piston.localPosition = Vector3.Lerp(_PistonDownTransform.localPosition, _pistonOriginalPosition, curveValue);
	}

	private void FixedUpdate () {
		// update firing system
		switch (_state) {
			case State.charging: {
				_slamTimer += Time.deltaTime;
				if (_slamTimer > _SlamPeriod) {
					_slamTimer = _SlamPeriod;
					_state = State.waitingOnActivation;
					_PistonTopPulser.StartPulsing();
				}
				break;
			}
			case State.waitingOnActivation: {
				break;
			}
		}
		// forget all enemies for next frame
		_detectedEnemies.Clear();
	}

	// parameters
	[Header("Refrences")]
	[SerializeField]
	private Transform _Piston;
	[SerializeField]
	private ScalePulser _PistonTopPulser;
	[SerializeField]
	private Transform _PistonDownTransform;
	[SerializeField]
	private EnemyDetector[] _EnemyDetectors;

	[Header("Attributes")]
	[SerializeField]
	private float _SlamPeriod;
	[SerializeField]
	private float _Damage;
	[SerializeField]
	private AnimationCurve _SlamReloadCurve;

	// internals
	private enum State {
		charging,
		waitingOnActivation,
	}
	private State _state;

	private float _slamTimer;
	private Vector3 _pistonOriginalPosition;
	private HashSet<IEnemy> _detectedEnemies;

}
