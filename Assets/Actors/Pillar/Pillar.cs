using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pillar : MonoBehaviour {

	// events
	private void Start () {
		_currentHp = _MaxHp;
		_initialGfxScale = _Gfx.localScale;
	}

	private void Update () {
		Vector3 newScale = _initialGfxScale;
		newScale.y = _initialGfxScale.y * _currentHp / _MaxHp;
		_Gfx.localScale = newScale;
		Vector3 newPosition = _Gfx.localPosition;
		newPosition.y = _Gfx.localScale.y / 2;
		_Gfx.localPosition = newPosition;
	}

	private void OnTriggerEnter (Collider other) {
		if (other.GetComponent<IEnemy>() != null) {
			_currentHp -= 1f;
			other.GetComponent<IEnemy>().CollidedWithPillar();
			if (_currentHp <= 0) {
				SceneManager.LoadScene("YouSuck");
			}
		}
	}

	// parameters
	[SerializeField]
	private float _MaxHp;
	[SerializeField]
	private Transform _Gfx;

	// internals
	private float _currentHp;
	private Vector3 _initialGfxScale;

}
