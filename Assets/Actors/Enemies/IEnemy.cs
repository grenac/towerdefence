using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemy {

	void Init (EnemySpawner newParentSpawner, Transform[] newWaypoints);
	void Damage (float damage);
	void CollidedWithPillar ();
	float GetTimeAlive ();
	bool IsAlive ();
	Transform GetTransform ();

}
