using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroPillarAnimator : MonoBehaviour {

	// events
	void Start () {
		_currentState = State.start;
		_timeActivated = 0;
		_initialPosition = transform.position;
	}

	void Update () {
		switch (_currentState) {

			case State.start: {
				break;
			}

			case State.animating: {
				float timeSince = Time.timeSinceLevelLoad - _timeActivated;
				if (timeSince > _AnimationInitialDelay) {
					if (timeSince < _AnimationDuration + _AnimationInitialDelay) {
						float curveValue = _AnimationCurve.Evaluate(( timeSince - _AnimationInitialDelay ) / _AnimationDuration);
						transform.position = Vector3.LerpUnclamped(_initialPosition, _AnimationTargetPosition, curveValue);
					} else {
						transform.position = _AnimationTargetPosition;
						_currentState = State.idle;
					}
				}
				break;
			}

			case State.idle: {
				break;
			}
		}
	}

	// interface
	public void ACTIVATE () {
		_timeActivated = Time.timeSinceLevelLoad;
		_currentState = State.animating;
	}

	// parameters
	[SerializeField]
	private Vector3 _AnimationTargetPosition;
	[SerializeField]
	private AnimationCurve _AnimationCurve;
	[SerializeField]
	private float _AnimationInitialDelay;
	[SerializeField]
	private float _AnimationDuration;

	// internals
	private enum State {
		start,
		animating,
		idle,
	}
	private State _currentState;

	private Vector3 _initialPosition;
	private float _timeActivated;

}
